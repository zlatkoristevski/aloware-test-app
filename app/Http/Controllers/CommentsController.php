<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Comment;

class CommentsController extends Controller
{
    private Comment $Comment;

    public function __construct(Comment $comment)
    {
        $this->comment = $comment;
    }

    public function index(Request $request)
    {
        return $this->comment->all();
    }
}
